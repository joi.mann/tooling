#!/bin/bash

## usage: *_generator.sh <lowercase_project_name>

## this script builds an Elixir Phoenix LiveView Application with Tailwind jit and Alpine JS 

## blixxir starts with golden:

## Give credit where credit is due: 
## Sergio Tapia https://sergiotapia.com/phoenix-160-liveview-esbuild-tailwind-jit-alpinejs-a-brief-tutorial
## https://github.com/sergiotapia/golden

PROJNAME=${1:-blixxir} 


echo "Prior to running this setup you may want to create a database and set DATABASE_URL="
echo "and set secret as well for production env"
echo
echo "For example, using posgres:"
echo "> sudo -u postgres psql"
echo "postgres=# create database $PROJNAME;"
echo "postgres=# create user $PROJNAME with encrypted password '<change_me>';"
echo "postgres=# grant all privileges on database $PROJNAME to $PROJNAME;"
echo "postgres=# exit"
echo ">"
#echo "# generate a really long secret"
#echo ">mix phx.gen.secret"
#echo "REALLY_LONG_SECRET"
#echo 
#echo "> export SECRET_KEY_BASE=REALLY_LONG_SECRET"
echo "> export DATABASE_URL=postgres://$PROJNAME:<chang_me>@localhost:5432/$PROJNAME"
echo
echo "CTRL+C to break out now and set up the database."
echo

read -p "Press any key to resume ..."


bakfile=''

backup_file() {

  filename="${bakfile}"
  destfile="${bakfile}.orig"
  
  if test -f "$destfile"; then
    echo "backup file <$destfile> exists --> making incremental backup."
    x=0
    tfile="$bakfile.$x.orig"
    while test -f "$tfile"
    do
      x=(x + 1)
      tfile="$bakfile.$x.orig"
    done
    mv "$destfile" "$tfile"
  fi
  if test -f "$filename"; then
    mv "$filename" "$destfile"
  fi
}

time mix phx.new "$PROJNAME" --live

#echo "FINISHED!!!"

cd "$PROJNAME"/assets

ls

npm install autoprefixer postcss postcss-import postcss-cli tailwindcss --save-dev
npm install alpinejs

ls

cd js

bakfile="app.js"
backup_file

awk 'BEGIN {x=0} {x = x + gsub("import \"../css/app.css\"", "// import \"../css/app.css\"", $0); print} END {print x | "cat 1>sub_app.js.success"}' app.js.orig > app.js

replacement_count=`cat sub_app.js.success`

if (( $replacement_count < 1 )); then
    RED='\033[0;31m'
    NC='\033[0m' # No Color
    #printf "I ${RED}love${NC} Stack Overflow\n"
    >&2 echo -e "${RED}substituting into app.js.orig failed!${NC}"
    rm sub_app.js.success
    exit 1
else
    echo "Success modifying app.js file - commenting out import app.css."
fi
rm sub_app.js.success




bakfile="app.js"
backup_file

read -r -d '' AWKVAR <<"EOF"
// BEGIN BLIXXIR INITIAL SETUP ROBOTIC INSERT
// import Alpine
import Alpine from "alpinejs";

window.Alpine = Alpine;
Alpine.start();

let hooks = {};
let liveSocket = new LiveSocket("/live", Socket, {
  params: { _csrf_token: csrfToken },
  hooks: hooks,
  dom: {
    onBeforeElUpdated(from, to) {
      if (from._x_dataStack) {
        window.Alpine.clone(from, to);
      }
    },
  },
});
//  END BLIXXIR INITIAL SETUP ROBOTIC INSERT 
EOF

awk -v var="$AWKVAR" '
BEGIN                { x = 0                                 }
 /^let liveSocket =/ { x = x + 1; print var                  }
!/^let liveSocket =/ { print                                 }
END                  { print x | "cat 1>sub2_app.js.success" }' app.js.orig > app.js

replacement_count=`cat sub2_app.js.success`

if (( $replacement_count < 1 )); then
    RED='\033[0;31m'
    NC='\033[0m' # No Color
    #printf "I ${RED}love${NC} Stack Overflow\n"
    >&2 echo -e "${RED}2nd substitution into app.js.orig failed!${NC}"
    rm sub2_app.js.success
    exit 1
else
    echo "Success modifying app.js file - integrating Alpine into app.js liveSocket."
fi
rm sub2_app.js.success




cd ..

bakfile="postcss.config.js"
backup_file

cat << EOF > postcss.config.js
module.exports = {
  plugins: {
    'postcss-import': {},
    tailwindcss: {},
    autoprefixer: {},
  }
}
EOF


bakfile="tailwind.config.js"
backup_file

cat << EOF > tailwind.config.js
module.exports = {
  mode: "jit",
  purge: ["./js/**/*.js", "../lib/*_web/**/*.*ex"],
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
EOF


cd css

bakfile="app.css"
backup_file

read -r -d '' INSTEXT <<"EOF"
@import "tailwindcss/base";
@import "tailwindcss/components";
@import "tailwindcss/utilities";
EOF

awk -v var="$INSTEXT" '
BEGIN                   { x = 0                                 }
 /@import "\.\/phoenix/ { print; x = x + 1; print var; next     }
!/@import "\.\/phoenix/ { print                                 }
END                     { print x | "cat 1>sub_app.css.success" }' app.css.orig > app.css

replacement_count=`cat sub_app.css.success`

if (( $replacement_count < 1 )); then
    RED='\033[0;31m'
    NC='\033[0m' # No Color
    #printf "I ${RED}love${NC} Stack Overflow\n"
    >&2 echo -e "${RED}substitution into app.css.orig failed!${NC}"
    rm sub_dev.exs.success
    exit 1
else
    echo "Success modifying app.css file - adding Tailwind imports."
fi
rm sub_app.css.success



cd ../../config




####################################################################################################################
######### This is not part of Sergios tutorial but I found I needed this to make it work thanks to SO        #######
######### https://stackoverflow.com/questions/65830776/mix-release-not-working-on-phoenix-live-view-demo-app #######
####################################################################################################################


bakfile="config.exs"
backup_file

awk '
BEGIN             { x = 0                                                             }
 /live_view: \[si/ { printf("%s%s\n",$0,","); x = x + 1; print "  server: true"; next }
!/live_view: \[si/ { print                                                            }
END               { print x | "cat 1>sub_config.exs.success"                          }' config.exs.orig > config.exs

replacement_count=`cat sub_config.exs.success`

if (( $replacement_count < 1 )); then
    RED='\033[0;31m'
    NC='\033[0m' # No Color
    #printf "I ${RED}love${NC} Stack Overflow\n"
    >&2 echo -e "${RED}substitution into config.exs.orig failed!${NC}"
    rm sub_config.exs.success
    exit 1
else
    echo "Success modifying config.exs file - adding: server: true."
fi
rm sub_config.exs.success

#######
#######  Also get a complaint during the build about missing config for esbuild so adding this in too
#######

echo ""                                      >> config.exs
echo "config :esbuild, :version, \"0.14.0\"" >> config.exs

#######
#######
#######
#######


bakfile="dev.exs"
backup_file

read -r -d '' DEVTEXT <<"EOF"
    npx: [
      "tailwindcss",
      "--input=css/app.css",
      "--output=../priv/static/assets/app.css",
      "--postcss",
      "--watch",
      cd: Path.expand("../assets", __DIR__)
    ]
EOF

awk -v var="$DEVTEXT" '
BEGIN                             { x = 0                                               }
 /esbuild: {Esbuild, :install_an/ { printf("%s%s\n",$0,","); x = x + 1; print var; next }
!/esbuild: {Esbuild, :install_an/ { print                                               }
END                               { print x | "cat 1>sub_dev.exs.success"               }' dev.exs.orig > dev.exs

replacement_count=`cat sub_dev.exs.success`

if (( $replacement_count < 1 )); then
    RED='\033[0;31m'
    NC='\033[0m' # No Color
    #printf "I ${RED}love${NC} Stack Overflow\n"
    >&2 echo -e "${RED}substitution into dev.exs.orig failed!${NC}"
    rm sub_dev.exs.success
    exit 1
else
    echo "Success modifying dev.exs file - adding Tailwind watcher."
fi
rm sub_dev.exs.success




cd ..
mix ecto.create




cd assets

bakfile="package.json"
backup_file

read -r -d '' PJSON <<"EOF"
"scripts": {
    "deploy": "NODE_ENV=production postcss css/app.css -o ../priv/static/assets/app.css"
  },
EOF

awk -v var="$PJSON" '
BEGIN                 { x = 0                                 }
 /"devDependencies":/ { x = x + 1; print var; print           }
!/"devDependencies":/ { print                                 }
END                   { print x | "cat 1>sub_package.json.success" }' package.json.orig > package.json

replacement_count=`cat sub_package.json.success`

if (( $replacement_count < 1 )); then
    RED='\033[0;31m'
    NC='\033[0m' # No Color
    #printf "I ${RED}love${NC} Stack Overflow\n"
    >&2 echo -e "${RED}substitution into package.json.orig failed!${NC}"
    rm sub_package.json.success
    exit 1
else
    echo "Success modifying package.json file - deploy to call postcss on css/app.css."
fi
rm sub_package.json.success




cd ..

bakfile="mix.exs"
backup_file

read -r -d '' ADPLY <<"EOF"
      "assets.deploy": [
        "cmd --cd assets npm run deploy",
        "esbuild default --minify",
        "phx.digest"
      ]
EOF

awk -v var="$ADPLY" '
BEGIN               { x = 0                                 }
 /"assets.deploy":/ { x = x + 1; print var; next            }
!/"assets.deploy":/ { print                                 }
END                 { print x | "cat 1>sub_mix.exs.success" }' mix.exs.orig > mix.exs

replacement_count=`cat sub_mix.exs.success`

if (( $replacement_count < 1 )); then
    RED='\033[0;31m'
    NC='\033[0m' # No Color
    #printf "I ${RED}love${NC} Stack Overflow\n"
    >&2 echo -e "${RED}substitution into mix.exs.orig failed!${NC}"
    rm sub_mix.exs.success
    exit 1
else
    echo "Success modifying mix.exs file - assets npm run deploy."
fi
rm sub_mix.exs.success


bakfile="build.sh"
backup_file

cat <<"EOF" > build.sh
#!/usr/bin/env bash
# exit on error
set -o errexit

# Install deps
npm install --prefix ./assets
mix deps.get --only prod

# Initial setup
MIX_ENV=prod mix assets.deploy
MIX_ENV=prod mix compile

# Migrate the database
MIX_ENV=prod mix ecto.migrate

# Build the release and overwrite the existing release directory
MIX_ENV=prod mix release --overwrite
EOF


scrt=`mix phx.gen.secret`
export SECRET_KEY_BASE=$scrt


chmod a+x build.sh

./build.sh

exit 0

"assets.deploy": [

