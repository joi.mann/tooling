# tooling

Scripts etc. to make work easier

## Phnx16LVesbTwAlp_generator.sh

- [ ] [Automates Phoenix Setup Detailed in This post by Sergio Tapia](https://sergiotapia.com/phoenix-160-liveview-esbuild-tailwind-jit-alpinejs-a-brief-tutorial)

This script generates a Phoenix 1.6 Liveview web application with Tailwind jit and Alpine JS

usage: Phnx16LVesbTwAlp_generator.sh <lower_case_project_name>
	


